/**
 * @see https://download.data.grandlyon.com/ws/rdata/jcd_jcdecaux.jcdvelov/all.json
 */
export interface Station {
  // number
  id: number;
  // name
  name: string;
  // address + "<br/>" + address2
  address: string;
  // commune
  town: string;
  // bike_stands
  totalBikeStandsNumber: number;
  // available_bikes
  availableBikesNumber: number;
  // available_bike_stands
  availableBikeStandsNumber: number;
  // status == "OPEN"
  isOpen: boolean;
  // availabilitycode (0 -> 3)
  // 0 = Grey | 1 = Green | 2 = Bleu | 3 = Orange
  // 0 == Unknown
  availability: number;
  // lng,lat
  coordinates: number[];
}
