import { Component } from '@angular/core';

import {NavController, Platform, ToastController, LoadingController, ModalController, Events} from 'ionic-angular';
import { StationsProvider } from '../../providers/stations';
import { Station } from '../../models/station';
import { MapModal } from '../modal_map/modal_map';
import { SettingsProvider } from '../../providers/settings';

declare const ol: any;

@Component({
  selector: 'page-map',
  templateUrl: 'map.html'
})
export class MapPage {
  protected map: any;
  protected stationsList: Station[];
  protected defaultStationStyle: any;
  protected stationStyles: any[];
  protected featuresList: {};

  /**
   * Creates a new tab with the map.
   *
   * @param navController A reference to the navigation controller.
   * @param toastController A reference to the toast controller.
   * @param platform A reference to the platform.
   * @param stationsProvider A reference to stations provider.
   * @param loadingController A reference to the loading controller.
   * @param modalController A reference to the modal controller.
   * @param settingsProvider A reference to the settings provider.
   * @param events A reference to the events system.
   */
  constructor(
    public navController: NavController, private toastController: ToastController, public platform: Platform,
    private stationsProvider: StationsProvider, public loadingController: LoadingController,
    public modalController : ModalController, private settingsProvider: SettingsProvider,
    private events: Events
  ) {
    // Initialize station styles
    this.defaultStationStyle = new ol.style.Style({
      image: new ol.style.Icon({
        anchor: [0.5, 0],
        anchorXUnits: 'fraction',
        anchorYUnits: 'pixels',
        opacity: 0.75,
        src: 'http://openlayers.org/en/v3.9.0/examples/data/icon.png'
      })
    });
    this.stationStyles = [];
    this.stationStyles[0] = new ol.style.Style({
      image: new ol.style.Icon({
        anchor: [0.5, 1],
        src : 'assets/markers/marker-grey.png'
      })
    });
    this.stationStyles[1] = new ol.style.Style({
      image: new ol.style.Icon({
        anchor: [0.5, 1],
        src : 'assets/markers/marker-green.png'
      })
    });
    this.stationStyles[2] = new ol.style.Style({
      image: new ol.style.Icon({
        anchor: [0.5, 1],
        src : 'assets/markers/marker-orange.png'
      })
    });
    this.stationStyles[3] = new ol.style.Style({
      image: new ol.style.Icon({
        anchor: [0.5, 1],
        src : 'assets/markers/marker-red.png'
      })
    });

    // Initialize other properties
    this.featuresList = {};

    // Register to the 'sync:update-needed' event
    this.events.subscribe(
      'sync:update-needed',
      (stations) => {
        this.doRefresh(stations)
      }
    );

    // Register to the 'map:move' event
    this.events.subscribe(
      'map:move',
      (coordinates) => {
        this.map.getView().setCenter(coordinates);
      }
    );
  }

  /**
   * Called when the view is done loading.
   */
  public ionViewDidLoad() {
    // Create map
    this.map = new ol.Map({
      target: 'map',
      layers: [
        new ol.layer.Tile({
            source: new ol.source.OSM()
        })
      ],
      view: new ol.View({
        center: ol.proj.fromLonLat([4.8789793, 45.7800045]),
        zoom: 15
      }),
      controls: []
    });

    // Load stations
    let loader = this.loadingController.create({
        content: 'Chargement en cours'
    });
    loader.present().then(
      () => {
        this.stationsProvider.load().subscribe(stations => {
          // Save stations
          this.stationsList = stations;

          // Create stations layer
          let stationsSource = new ol.source.Vector({});

          for (let i = 0 ; i< this.stationsList.length; i++) {
            // Create a feature for each station
            let stationFeature = new ol.Feature({
              geometry : new ol.geom.Point(this.stationsList[i].coordinates),
              index: i,
              id: this.stationsList[i].id,
              name: this.stationsList[i].name,
              availability: this.stationsList[i].availability
            });

            // Store the station's feature so as to update it in the future if necessary
            this.featuresList[this.stationsList[i].id] = stationFeature;

            // Add the newly created feature to the stations layer
            stationsSource.addFeature(stationFeature);
          }

          this.map.addLayer(new ol.layer.Vector({
            source: stationsSource,
            style: (feature, resolution) => {
              if(feature.get('availability') >= 0 && feature.get('availability') <= 3) {
                return this.stationStyles[feature.get('availability')];
              }
              else {
                return this.defaultStationStyle;
              }
            }
          }));

          // Map is done loading
          loader.dismiss();
        });
      },
      (error) => {
          console.error('Couldn\'t display loader: ', error);
      }
    );

    // Register click event on map
    this.map.on(
      'click',
      (e) => {
        this.map.forEachFeatureAtPixel(
          e.pixel,
          (feature, layer) => {
            this.settingsProvider.getFavoriteStations().then(
              (favoritesList) => {
                let modal = this.modalController.create(
                  MapModal,
                  {station: this.stationsList[feature.get('index')], isFavorite: favoritesList.indexOf(feature.get('id')) > -1}
                );
                modal.present();
              },
              (error) => {
                console.error('Couldn\'t fetch favorite stations: ', error);
              }
            );
          },
          {hitTolerance: 15}
        )
      }
    )
  }

  /**
   * Called when the user clicks on the "Locate me" button.
   *
   * @param item A reference to the clicked item.
   */
  public locateMe(item) {
    this.platform.ready().then(
      () => {
        navigator.geolocation.getCurrentPosition(
          (response) => {
            this.map.getView().setCenter(ol.proj.fromLonLat([response.coords.longitude, response.coords.latitude]));
          },
          (error) => {
            // Inform user there was an error
            let toast = this.toastController.create({
              message: 'Une erreur est survenue lors de la géolocalisation.',
              duration: 1500,
              showCloseButton: true,
              closeButtonText: 'Fermer'
            });
            toast.present();

            // Log error to console
            console.error('Couldn\'t fetch geolocation: ', error);
          },
          {timeout: 5000}
        );
      },
      () => {
        let toast = this.toastController.create({
          message: 'Fonctionnalité non disponible.',
          duration: 1500,
          showCloseButton: true,
          closeButtonText: 'Fermer'
        });
        toast.present();
      }
    );
  }

  /**
   * Triggered by the 'sync:update-needed' event.
   *
   * @param stations The updated list of stations.
   */
  protected doRefresh(stations: Station[]) {
    // Store updated stations
    this.stationsList = stations;

    // Update each station's feature
    for(let i = 0; i < stations.length; i++) {
      this.featuresList[this.stationsList[i].id].set('availability', this.stationsList[i].availability);
    }
  }
}
