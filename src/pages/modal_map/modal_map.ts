import { Component } from '@angular/core';
import { ViewController, NavParams, ToastController } from 'ionic-angular';
import { Station } from '../../models/station';
import { SettingsProvider } from '../../providers/settings';
import { MyApp } from '../../app/app.component';

@Component({
    selector: 'modal-map',
    templateUrl: 'modal_map.html'
})
export class MapModal {
  protected station: Station;
  protected isFavorite: boolean;
  protected favoriteText: string;

  /**
   * Creates a new modal about a station.
   *
   * @param viewController A reference to the view controller.
   * @param toastController A reference to the toast controller.
   * @param navParams A reference to the navigation parameters.
   * @param settingsProvider A reference to the settings provider.
   */
  constructor(
    private viewController: ViewController, private toastController: ToastController,
    private navParams: NavParams, private settingsProvider: SettingsProvider
  ) {
    this.station = navParams.get('station');
    this.isFavorite = navParams.get('isFavorite') || false;
    this.favoriteText = this.isFavorite ? 'Retirer des favoris' : 'Ajouter aux favoris';
  }

  /**
   * Called when the user clicks on the favorites button.
   *
   * @param item A reference to the clicked item.
   */
  public handleFavorite(item) {
    this.settingsProvider.getFavoriteStations().then(
      (favoritesList) => {
        favoritesList = favoritesList != null ? favoritesList : MyApp.DEFAULT_FAVORITE_STATIONS;
        let stationIndex = favoritesList.indexOf(this.station.id);
        let toastContents;

        if(stationIndex == -1) {
          favoritesList.push(this.station.id);

          toastContents = 'La station ' + this.station.name + ' a été ajoutée aux favoris.';
          this.isFavorite = true;
          this.favoriteText = 'Retirer des favoris';
        }
        else {
          favoritesList.splice(stationIndex, 1);

          toastContents = 'La station ' + this.station.name + ' a été retirée des favoris.';
          this.isFavorite = false;
          this.favoriteText = 'Ajouter aux favoris';
        }

        this.settingsProvider.setFavoriteStations(favoritesList);
        let toast = this.toastController.create({
          message: toastContents,
          duration: 1500,
          showCloseButton: true,
          closeButtonText: 'Fermer'
        });
        toast.present();
      },
      (error) => {
        console.error('Couldn\'t fetch favorite stations: ', error);
      }
    )
  }

  /**
   * Called when the user clicks on the "Fermer" button.
   *
   * @param item A reference to the clicked item.
   */
  public dismiss(item) {
    this.viewController.dismiss();
  }
}
