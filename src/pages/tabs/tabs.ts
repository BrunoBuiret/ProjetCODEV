import { Component } from '@angular/core';
import { MapPage } from '../map/map';
import { SettingsPage } from '../settings/settings';
import { FavoriteStationsPage } from '../favorite_stations/favorite_stations';

@Component({
  templateUrl: 'tabs.html'
})

export class TabsPage {
  // this tells the tabs component which Pages
  // should be each tab's root Page
  tab1Root: any = SettingsPage;
  tab2Root: any = MapPage;
  tab3Root: any = FavoriteStationsPage;

  public static readonly TAB_MAP_INDEX = 1;

  constructor() {}
}
