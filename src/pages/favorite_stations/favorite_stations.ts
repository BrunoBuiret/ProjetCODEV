import { Component} from '@angular/core';
import { NavController, LoadingController, ModalController, ToastController, Events } from 'ionic-angular';
import { MyApp } from '../../app/app.component';
import { Station } from '../../models/station';
import { StationsProvider } from '../../providers/stations';
import { SettingsProvider } from '../../providers/settings';
import { MapModal } from '../modal_map/modal_map';
import { TabsPage } from '../tabs/tabs';

@Component({
  selector: 'page-favorite-stations',
  templateUrl: 'favorite_stations.html'
})
export class FavoriteStationsPage {
  protected favoriteStationsList: Station[];
  protected favoriteStationsNumber: number[];
  protected availabilityIcons: string[] = [
    'help',
    'checkmark',
    'warning',
    'flash'
  ];
  protected availabilityColors: string[] = [
    'status-unknown',
    'status-green',
    'status-orange',
    'status-red'
  ];

  /**
   * Creates a new tab with the favorite stations.
   *
   * @param navController A reference to the navigation controller.
   * @param loadingController A reference to the loading controller.
   * @param toastController A reference to the toast controller.
   * @param modalController A reference to the modal controller.
   * @param stationsProvider A reference to stations provider.
   * @param settingsProvider A reference to the settings provider.
   * @param events A reference to the events system.
   */
  constructor(
    public navController: NavController, public loadingController: LoadingController,
    public toastController: ToastController, public modalController: ModalController,
    private stationsProvider: StationsProvider, private settingsProvider: SettingsProvider,
    public events: Events
  ) {
    // Load favorite stations
    let loader = this.loadingController.create({
      content: 'Chargement en cours...'
    });
    loader.present().then(
      () => {
        this.stationsProvider.load().subscribe(stations => {
          this.loadFavoriteStations(stations);
          loader.dismiss();
        });
      },
      (error) => {
        console.error('Couldn\'t display loader: ', error);
      }
    );

    // Register to the 'sync:update-needed' event
    this.events.subscribe(
      'sync:update-needed',
      (stations) => {
        this.loadFavoriteStations(stations);
      }
    );
  }

  /**
   * Called when the user pulls the stations' list to refresh it.
   *
   * @param refresher A reference to the refresher object used to notice the user.
   * @todo Find a way for the refresher to wait for the end of synchronization.
   */
  public doRefresh(refresher) {
    this.stationsProvider.load(true).subscribe(stations => {
      this.events.publish('sync:update-needed', stations);
      refresher.complete();
    });
  }

  /**
   * Called when the user clicks on a favorite station item.
   *
   * @param item A reference to the clicked item.
   * @param stationId The station's id.
   */
  public itemClicked(item, stationId: number) {
    let station = this.findStationById(stationId);

    if(station != null) {
      this.events.publish('map:move', station.coordinates);
      this.navController.parent.select(TabsPage.TAB_MAP_INDEX);
    }
    else {
      let toast = this.toastController.create({
        message: 'Station inconnue',
        duration: 1500,
        showCloseButton: true,
        closeButtonText: 'Fermer'
      });
      toast.present();
    }
  }

  /**
   * Called when the user clicks on the "Details" button.
   *
   * @param item A reference to the clicked item.
   * @param stationId The station's id.
   */
  public showDetails(item, stationId: number) {
    let station: Station = this.findStationById(stationId);

    if(station != null) {
      let modal = this.modalController.create(
        MapModal,
        {station: station, isFavorite: true}
      );
      modal.present();
    }
    else {
      let toast = this.toastController.create({
        message: 'Station inconnue',
        duration: 1500,
        showCloseButton: true,
        closeButtonText: 'Fermer'
      });
      toast.present();
    }
  }

  /**
   * Called when the user clicks on the "Delete" button.
   *
   * @param item A reference to the clicked item.
   * @param stationId The station's id.
   */
  public unfavoriteStation(item, stationId: number) {
    let loader = this.loadingController.create({
      content: 'Suppression des favoris en cours...'
    });
    loader.present().then(
      () => {
        // Remove the station from the list
        let stationIndex = this.favoriteStationsNumber.indexOf(stationId);

        if(stationIndex > -1) {
          this.favoriteStationsList.splice(stationIndex, 1);
          this.favoriteStationsNumber.splice(stationIndex, 1);
          this.settingsProvider.setFavoriteStations(this.favoriteStationsNumber);
        }
        else {
          console.error('There is no station #' + stationId + ' in the user\'s favorites.');
        }

        loader.dismiss();
      },
      (error) => {
        console.error('Couldn\'t display loader: ', error);
      }
    );
  }

  /**
   * Finds a station by its id in the favorites list.
   *
   * @param stationId The station's id.
   * @returns Station|null The station if it exists and if it is a favorite, {@code null} otherwise.
   */
  private findStationById(stationId: number) {
    for(let i = 0; i < this.favoriteStationsList.length; i++) {
      if(this.favoriteStationsList[i].id == stationId) {
        return this.favoriteStationsList[i];
      }
    }

    return null;
  }

  /**
   * Filters the list of stations to retain only the favorite ones.
   *
   * @param stations The full list of stations.
   */
  private loadFavoriteStations(stations: Station[]) {
    this.settingsProvider.getFavoriteStations().then(
      (favoriteStationsNumber: number[]) => {
        this.favoriteStationsNumber = favoriteStationsNumber != null ? favoriteStationsNumber : MyApp.DEFAULT_FAVORITE_STATIONS;

        // Filter every available stations
        this.favoriteStationsList = stations.filter(
          (value: Station, index: number, values: Station[]) => {
            return this.favoriteStationsNumber.indexOf(value.id) > -1;
          },
          this
        );
      },
      (error) => {
        console.error('Couldn\'t fetch favorite stations: ', error);
      }
    );
  }
}
