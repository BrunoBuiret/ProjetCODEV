import { Component } from '@angular/core';
import {NavController, Events} from 'ionic-angular';
import { SettingsProvider } from '../../providers/settings';
import { MyApp } from '../../app/app.component';

@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html'
})
export class SettingsPage {
  protected syncInterval: number;

  /**
   * Creates a new tab with the settings.
   *
   * @param navCtrl A reference to the navigation controller.
   * @param events A reference to the events system.
   * @param settingsProvider A reference to the settings provider.
   */
  constructor(public navCtrl: NavController, public events: Events, public settingsProvider: SettingsProvider) {
    // Fetch sync interval
    this.settingsProvider.getSyncInterval().then(
        (value: number) => {
          this.syncInterval = value != null ? value : MyApp.DEFAULT_SYNC_INTERVAL;
        },
        (error) => {
          console.error('Couldn\'t fetch sync interval from storage: ', error);
        }
    );
  }

  /**
   * Called when the user changes the sync interval.
   *
   * @param syncInterval The new sync interval value.
   */
  public onSyncIntervalChange(syncInterval) {
    this.settingsProvider.setSyncInterval(syncInterval);
    this.events.publish('settings:sync-interval-changed', syncInterval);
  }
}
