import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs';
import {Station} from '../models/station';

declare var ol: any;

/*
  Generated class for the StationsProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class StationsProvider {
  stations: Observable<Station[]> = null;

  /**
   * Creates a new provider to fetch the stations from the Grand Lyon.
   *
   * @param http A reference to the HTTP system.
   */
  constructor(public http: Http) {
  }

  /**
   * Loads the stations from the Grand Lyon.
   *
   * @param forceRefresh If {@code true}, ncessarily fetch the stations from the internet instead of using a cached
   * version.
   * @returns {Observable<Station[]>} A promise to be used to get the stations.
   */
  public load(forceRefresh: boolean = false): Observable<Station[]> {
    // Have the stations already been loaded?
    if(this.stations != null && !forceRefresh) {
      return this.stations;
    }

    // Perform HTTP GET call to fetch the stations' status
    this.stations = this.http.get('https://download.data.grandlyon.com/ws/rdata/jcd_jcdecaux.jcdvelov/all.json')
      .map(function(response): Station[] {
        let stations = [], raw = response.json().values;

        for(let i = 0; i < raw.length; i++) {
          let station = {
            id: parseInt(raw[i]['number']),
            name: raw[i]['name'],
            address: raw[i]['address'],
            // http://stackoverflow.com/questions/4878756/javascript-how-to-capitalize-first-letter-of-each-word-like-a-2-word-city#answer-4878800
            town: raw[i]['commune'].replace(
              /\S+/g,
              function(input) {
                if(input != "er" && input != "ème") {
                  return input.charAt(0).toLocaleUpperCase() + input.substr(1).toLocaleLowerCase();
                }

                return input;
              }
            ),
            coordinates: ol.proj.transform([parseFloat(raw[i]['lng']), parseFloat(raw[i]['lat'])], 'EPSG:4326', 'EPSG:3857'),
            totalBikeStandsNumber: parseInt(raw[i]['bike_stands']),
            availableBikesNumber: parseInt(raw[i]['available_bikes']),
            availableBikeStandsNumber: parseInt(raw[i]['available_bike_stands']),
            isOpen: raw[i]['status'] == 'OPEN',
            availability: parseInt(raw[i]['availabilitycode'])
          };

          if(raw[i].address2 != 'None') {
            station.address += ' - ' + raw[i].address2;
          }

          stations.push(station);
        }

        return stations;
      })
    ;

    return this.stations;
  }
}
