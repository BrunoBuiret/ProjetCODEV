import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable()
export class SettingsProvider {
  private storage: Storage;

  /**
   * Initializes the storage provider.
   */
  constructor() {
    this.storage = new Storage();
  }

  /**
   * Gets the sync interval.
   *
   * @returns {Promise<any>} A promise to use to fetch the value.
   */
  public getSyncInterval() {
    return this.storage.get('syncInterval');
  }

  /**
   * Sets the sync interval.
   *
   * @param syncInterval The sync interval value.
   */
  public setSyncInterval(syncInterval: number) {
    this.storage.set('syncInterval', syncInterval);
  }

  /**
   * Gets the favorite stations.
   *
   * @returns {Promise<any>}
   */
  public getFavoriteStations() {
    return this.storage.get('favoriteStationsList');
  }

  /**
   * Sets the favorite stations.
   *
   * @param favoriteStations
   */
  public setFavoriteStations(favoriteStations: number[]) {
    this.storage.set('favoriteStationsList', favoriteStations);
  }
}
