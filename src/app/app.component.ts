import { Component } from '@angular/core';
import { Platform, Events } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';
import { SettingsProvider } from '../providers/settings';
import { TabsPage } from '../pages/tabs/tabs';
import { StationsProvider } from '../providers/stations';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  static readonly DEFAULT_SYNC_INTERVAL = 0;
  static readonly DEFAULT_FAVORITE_STATIONS = [];

  rootPage = TabsPage;
  protected synchronizationTimer: any;

  constructor(
    private platform: Platform, private events: Events, public settingsProvider: SettingsProvider,
    private stationsProvider: StationsProvider
  ) {
    // Initialize properties
    this.synchronizationTimer = null;

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
      Splashscreen.hide();

      // Initialize timer for synchronization
      this.events.subscribe(
        'settings:sync-interval-changed',
        (newInterval) => {
          // If there already is a timer, clear it
          if(this.synchronizationTimer != null) {
            clearInterval(this.synchronizationTimer);
            this.synchronizationTimer = null;
          }

          // Launch new timer if necessary
          if(newInterval > 0) {
            this.synchronizationTimer = setInterval(
              () => {
                this.stationsProvider.load(true).subscribe((stations) => {
                  this.events.publish('sync:update-needed', stations);
                });
              },
              newInterval * 1000 * 60
            );
          }
        }
      );

      // Initialize default settings
      settingsProvider.getSyncInterval().then(
        (value) => {
          if(value == null) {
            settingsProvider.setSyncInterval(MyApp.DEFAULT_SYNC_INTERVAL);
          }
          else {
            this.events.publish('settings:sync-interval-changed', value);
          }
        },
        (error) => {
          console.error('Couldn\'t fetch sync interval from storage: ', error);
        }
      );

      settingsProvider.getFavoriteStations().then(
        (value) => {
          if(value == null) {
            settingsProvider.setFavoriteStations(MyApp.DEFAULT_FAVORITE_STATIONS);
          }
        },
        (error) => {
          console.error('Couldn\'t fetch favorite stations: ', error);
        }
      )
    });
  }
}
