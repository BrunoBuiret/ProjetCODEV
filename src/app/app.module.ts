import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { MapPage } from '../pages/map/map';
import { MapModal } from '../pages/modal_map/modal_map';
import { TabsPage } from '../pages/tabs/tabs';
import { SettingsPage } from '../pages/settings/settings';
import { FavoriteStationsPage } from '../pages/favorite_stations/favorite_stations';
import { SettingsProvider } from '../providers/settings';
import { StationsProvider } from '../providers/stations';

@NgModule({
  declarations: [
    MyApp,
    MapPage,
    MapModal,
    TabsPage,
    SettingsPage,
    FavoriteStationsPage
  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    MapPage,
    MapModal,
    TabsPage,
    SettingsPage,
    FavoriteStationsPage
  ],
  providers: [{provide: ErrorHandler, useClass: IonicErrorHandler}, SettingsProvider, StationsProvider]
})
export class AppModule {}
